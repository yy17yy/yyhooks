import { onUnmounted, ref, watch } from 'vue';

const useClickAway = (onClickAway) => {
  const target = ref();

  const handleClick = (event) => {
    if (target.value && !target.value.contains(event.target)) {
      onClickAway(event);
    }
  }

  useWindowEvent('click', handleClick);

  return target;
}

export default useClickAway;
