import { onUnmounted } from "@vue/runtime-core";

const useWindowEvent = (event, onEvent, options) => {
  window.addEventListener(event, onEvent, options);
  onUnmounted(() => window.removeEventListener(event, onEvent));
}