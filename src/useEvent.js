import { watch } from 'vue';


const useEvent = (event, onEvent, options) => {
  const target = ref();
  watch(target, (a, b, cleanUp) => {
    if (target.value) {
      const targetElement = target.value;
      targetElement.addEventListener(event, onEvent, options);
      cleanUp(() => targetElement.removeEventListener(event, onEvent));
    }
  })

  return target;
}

export default useEvent;