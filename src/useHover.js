import { watch } from "@vue/runtime-core";

const useHover = () => {
  const target = ref();
  const hovered = ref(false);

  const handleMouseEnter = () => hovered.value = true;
  const handleMouseLeave = () => hovered.value = false;

  watch(target, (a, b, cleanUp) => {
    if (target.value) {
      const targetElement = target.value;
      targetElement.addEventListener('mousemove', handleMouseEnter);
      targetElement.addEventListener('mouseleave', handleMouseLeave);
      cleanUp(() => {
        targetElement.removeEventListener('mousemove', handleMouseEnter);
        targetElement.removeEventListener('mouseleave', handleMouseLeave);
      })
    }
  })

  return [target, hovered];
}

export default useHover;